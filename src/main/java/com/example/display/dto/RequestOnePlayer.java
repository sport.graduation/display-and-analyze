package com.example.display.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RequestOnePlayer implements Serializable {
    private long playerID;
    private long startTimeMillis;

}
