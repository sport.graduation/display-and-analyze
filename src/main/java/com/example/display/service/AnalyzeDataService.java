package com.example.display.service;

import com.example.display.dto.RequestForAnalyze;
import com.example.display.model.AnalyzedPracticeData;
import com.example.display.model.PracticeData;
import com.example.display.model.analyzeddata.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnalyzeDataService {
    @Autowired
    RestTemplate restTemplate;

    public PracticeData[] getPlayerDataForAnalyzing(RequestForAnalyze request){
        String url = "http://localhost:9200/api/v1/info/practice/one/toAnalyze";
        HttpEntity<RequestForAnalyze> requestEntity = new HttpEntity<>(request);
        ResponseEntity<PracticeData[]> entity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, PracticeData[].class);
        return entity.getBody();
    }

    public AnalyzedPracticeData analyzeData(RequestForAnalyze request){
        List<PracticeData> currentWeek = List.of(getPlayerDataForAnalyzing(request));
        List<PracticeData> previousWeek = List.of(getPlayerDataForAnalyzing(new RequestForAnalyze(request.getPlayerID(), request.getStartTimeMillis()-604800000,request.getEndTimeMillis()-604800000)));
        PracticeData previousWeekSum = calculateSumOfPracticeDataList(previousWeek);
        PracticeData currentWeekSum = calculateSumOfPracticeDataList(currentWeek);
        AnalyzedPracticeData analyzedData = setInfoToAnalyzedDataObj(currentWeek);
        analyzedData.setTotalDistanceImprovement("Current week total distance increased " + (currentWeekSum.getTotalDistance() - previousWeekSum.getTotalDistance()) + " Meters compared to last week.");
        analyzedData.setBurnedCaloriesImprovement("Current week burned calories increased " + (currentWeekSum.getBurnedCalories() - previousWeekSum.getBurnedCalories()) + " kcals compared to last week.");
        analyzedData.setHsrImprovement("Current week High Speed Running minutes increased " + (currentWeekSum.getHsr() - previousWeekSum.getHsr()) + " min compared to last week.");
        analyzedData.setMaxSpeedImprovement("Current week max Speed increased " + ((double)100*(currentWeekSum.getMaxSpeed() - previousWeekSum.getMaxSpeed())/previousWeekSum.getMaxSpeed()) + "% compared to last week.");////////////////
        analyzedData.setAvgHeartRateImprovement("Current week avg HR increased " + ((double)100*(currentWeekSum.getAvgHeartRate() - previousWeekSum.getAvgHeartRate())/previousWeekSum.getMinHeartRate()) + "% compared to last week.");
        analyzedData.setMaxHeartRateImprovement("Current week max HR increased " + ((double)100*(currentWeekSum.getMaxHeartRate() - previousWeekSum.getMaxHeartRate())/previousWeekSum.getMinHeartRate()) + "% compared to last week.");
        analyzedData.setMinHeartRateImprovement("Current week min HR increased " + ((double)100*(currentWeekSum.getMinHeartRate() - previousWeekSum.getMinHeartRate())/previousWeekSum.getMinHeartRate()) + "% compared to last week.");
        analyzedData.setRedZoneDurationImprovement("Current week red zone duration increased " + ((double)100*(currentWeekSum.getRedZoneDuration() - previousWeekSum.getRedZoneDuration())/previousWeekSum.getRedZoneDuration()) + "% compared to last week.");//////////////
        analyzedData.setNumOfSprintsImprovement("Current week num of sprints increased " + ((double)100*(currentWeekSum.getNumOfSprints() - previousWeekSum.getNumOfSprints())/previousWeekSum.getNumOfSprints()) + "% compared to last week.");////////////////
        return analyzedData;
    }

    public PracticeData calculateSumOfPracticeDataList(List<PracticeData> practiceDataList){
        PracticeData practiceData =new PracticeData();
        for (PracticeData data:practiceDataList){
            practiceData.setBurnedCalories(data.getBurnedCalories()+practiceData.getBurnedCalories());
            practiceData.setHsr(data.getHsr()+practiceData.getHsr());
            practiceData.setMaxSpeed(data.getMaxSpeed()+practiceData.getMaxSpeed());
            practiceData.setMaxHeartRate(data.getMaxHeartRate()+practiceData.getMaxHeartRate());
            practiceData.setMinHeartRate(data.getMinHeartRate()+practiceData.getMinHeartRate());
            practiceData.setAvgHeartRate(data.getAvgHeartRate()+ practiceData.getAvgHeartRate());
            practiceData.setTotalDistance(data.getTotalDistance()+ practiceData.getTotalDistance());
            practiceData.setNumOfSprints(data.getNumOfSprints()+practiceData.getNumOfSprints());
            practiceData.setRedZoneDuration(data.getRedZoneDuration()+ practiceData.getRedZoneDuration());
        }
        practiceData.setBurnedCalories(practiceData.getBurnedCalories());
        practiceData.setHsr(practiceData.getHsr());
        practiceData.setMaxSpeed(practiceData.getMaxSpeed()/practiceDataList.size());
        practiceData.setMaxHeartRate(practiceData.getMaxHeartRate()/practiceDataList.size());
        practiceData.setMinHeartRate(practiceData.getMinHeartRate()/practiceDataList.size());
        practiceData.setAvgHeartRate(practiceData.getAvgHeartRate()/practiceDataList.size());
        practiceData.setTotalDistance(practiceData.getTotalDistance());
        practiceData.setNumOfSprints(practiceData.getNumOfSprints()/practiceDataList.size());
        practiceData.setRedZoneDuration(practiceData.getRedZoneDuration()/practiceDataList.size());
        return practiceData;
    }
    public AnalyzedPracticeData setInfoToAnalyzedDataObj(List<PracticeData> practiceDataList){
        AnalyzedPracticeData analyzedData = new AnalyzedPracticeData();
        analyzedData.setPlayerID(practiceDataList.get(0).getPlayerID());
          List<TotalDistance> totalDistance =new ArrayList<>();
          List<MaxSpeed> maxSpeed=new ArrayList<>();
          List<BurnedCalories> burnedCalories=new ArrayList<>() ;
          List<NumOfSprints> numOfSprints=new ArrayList<>() ;
          List<HSR> hsr =new ArrayList<>();
          List<AvgHeartRate> avgHeartRate=new ArrayList<>();
          List<MaxHeartRate> maxHeartRate=new ArrayList<>();
          List<MinHeartRate> minHeartRate=new ArrayList<>();
          List<RedZoneDuration> redZoneDuration=new ArrayList<>();
          long startTimeMillis;
          long endTimeMillis;
        for (PracticeData data : practiceDataList) {
            startTimeMillis = data.getStartTimeMillis();
            endTimeMillis = data.getEndTimeMillis();
            totalDistance.add(new TotalDistance(data.getTotalDistance(),startTimeMillis,endTimeMillis));
            maxSpeed.add(new MaxSpeed(data.getMaxSpeed(), startTimeMillis,endTimeMillis));
            burnedCalories.add(new BurnedCalories(data.getBurnedCalories(),startTimeMillis,endTimeMillis));
            numOfSprints.add(new NumOfSprints(data.getNumOfSprints(), startTimeMillis,endTimeMillis));
            hsr.add(new HSR(data.getHsr(), startTimeMillis,endTimeMillis));
            avgHeartRate.add(new AvgHeartRate(data.getAvgHeartRate(), startTimeMillis,endTimeMillis));
            maxHeartRate.add(new MaxHeartRate(data.getMaxHeartRate(), startTimeMillis,endTimeMillis));
            minHeartRate.add(new MinHeartRate(data.getMinHeartRate(), startTimeMillis,endTimeMillis));
            redZoneDuration.add(new RedZoneDuration(data.getRedZoneDuration(), startTimeMillis,endTimeMillis));
        }
        analyzedData.setAvgHeartRateList(avgHeartRate);
        analyzedData.setBurnedCaloriesList(burnedCalories);
        analyzedData.setHsrList(hsr);
        analyzedData.setTotalDistanceList(totalDistance);
        analyzedData.setMaxSpeedList(maxSpeed);
        analyzedData.setNumOfSprintsList(numOfSprints);
        analyzedData.setMaxHeartRateList(maxHeartRate);
        analyzedData.setMinHeartRateList(minHeartRate);
        analyzedData.setRedZoneDurationList(redZoneDuration);

        return analyzedData;
    }
}
