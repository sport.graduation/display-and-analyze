package com.example.display.service;

import com.example.display.dto.RequestMultiPlayers;
import com.example.display.dto.RequestOnePlayer;
import com.example.display.dto.RequestTeam;
import com.example.display.model.DailyData;
import com.example.display.model.PracticeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PlayerDailyDataService {
    @Autowired
    RestTemplate restTemplate;

    public DailyData getOnePlayer(RequestOnePlayer request){
        String url = "http://localhost:9200/api/v1/info/daily/one";
        HttpEntity<RequestOnePlayer> requestEntity = new HttpEntity<>(request);
        ResponseEntity<DailyData> entity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, DailyData.class);
        return entity.getBody();
    }
    public DailyData[] getMultiplePlayer(RequestMultiPlayers request){
        String url = "http://localhost:9200/api/v1/info/daily/multiplayer";
        HttpEntity<RequestMultiPlayers> requestEntity = new HttpEntity<>(request);
        ResponseEntity<DailyData[]> entity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, DailyData[].class);
        return entity.getBody();
    }
    public DailyData[] getTeam(RequestTeam request){
        String url = "http://localhost:9200/api/v1/info/daily/team";
        HttpEntity<RequestTeam> requestEntity = new HttpEntity<>(request);
        ResponseEntity<DailyData[]> entity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, DailyData[].class);
        return entity.getBody();
    }

}
