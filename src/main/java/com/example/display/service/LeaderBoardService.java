package com.example.display.service;

import com.example.display.dto.RequestTeam;
import com.example.display.model.Leaderboards;
import com.example.display.model.PracticeData;
import com.example.display.model.leaderboards.HsrLeaderboards;
import com.example.display.model.leaderboards.MaxSpeedLeaderboards;
import com.example.display.model.leaderboards.TotalDistanceLeaderboards;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class LeaderBoardService {

    @Autowired
    PlayerPracticeDataService practiceDataService;

    public Leaderboards leaderboards(RequestTeam requestTeam) {
        List<PracticeData> practiceDataList = new java.util.ArrayList<>(List.of(practiceDataService.getTeam(requestTeam)));

        practiceDataList.sort(Comparator.comparingDouble(PracticeData::getTotalDistance).reversed());
        List<TotalDistanceLeaderboards> distanceLeaderboards = getTotalDistanceLeaderboards(practiceDataList);

        practiceDataList.sort(Comparator.comparingDouble(PracticeData::getMaxSpeed).reversed());
        List<MaxSpeedLeaderboards> maxSpeedLeaderboards = getMaxSpeedLeaderboards(practiceDataList);

        practiceDataList.sort(Comparator.comparingDouble(PracticeData::getHsr).reversed());
        List<HsrLeaderboards> hsrLeaderboards = getHsrLeaderboards(practiceDataList);

        Leaderboards leaderboards = new Leaderboards();
        leaderboards.setHsrLeaderboards(hsrLeaderboards);
        leaderboards.setMaxSpeedLeaderboards(maxSpeedLeaderboards);
        leaderboards.setTotalDistanceLeaderboards(distanceLeaderboards);

        return leaderboards;
    }


    public List<MaxSpeedLeaderboards> getMaxSpeedLeaderboards(List<PracticeData> practiceDataList) {
        List<MaxSpeedLeaderboards> list = new ArrayList<>();
        for (PracticeData data : practiceDataList) {
            MaxSpeedLeaderboards leaderboards = new MaxSpeedLeaderboards();
            System.out.println(data);
            leaderboards.setPlayerID(data.getPlayerID());
            leaderboards.setMaxSpeed(data.getMaxSpeed());
            list.add(leaderboards);
        }
        return list;
    }
    public List<HsrLeaderboards> getHsrLeaderboards(List<PracticeData> practiceDataList) {
        List<HsrLeaderboards> list = new ArrayList<>();
        for (PracticeData data : practiceDataList) {
            HsrLeaderboards leaderboards = new HsrLeaderboards();
            System.out.println(data);
            leaderboards.setPlayerID(data.getPlayerID());
            leaderboards.setHsr(data.getHsr());
            list.add(leaderboards);
        }
        return list;
    }
    public List<TotalDistanceLeaderboards> getTotalDistanceLeaderboards(List<PracticeData> practiceDataList) {
        List<TotalDistanceLeaderboards> list = new ArrayList<>();
        for (PracticeData data : practiceDataList) {
            TotalDistanceLeaderboards leaderboards = new TotalDistanceLeaderboards();
            System.out.println(data);
            leaderboards.setPlayerID(data.getPlayerID());
            leaderboards.setTotalDistance(data.getTotalDistance());
            list.add(leaderboards);
        }
        return list;
    }

}
