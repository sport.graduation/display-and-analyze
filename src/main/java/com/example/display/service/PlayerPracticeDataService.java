package com.example.display.service;

import com.example.display.dto.RequestMultiPlayers;
import com.example.display.dto.RequestOnePlayer;
import com.example.display.dto.RequestTeam;
import com.example.display.model.PracticeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PlayerPracticeDataService {
    @Autowired
    RestTemplate restTemplate;

    public PracticeData getOnePlayer(RequestOnePlayer request){
        String url = "http://INFORMATION/api/v1/info/practice/one";
        HttpEntity<RequestOnePlayer> requestEntity = new HttpEntity<>(request);
        ResponseEntity<PracticeData> entity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, PracticeData.class);
        return entity.getBody();
    }
    public PracticeData[] getMultiplePlayer(RequestMultiPlayers request){
        String url = "http://INFORMATION/api/v1/info/practice/multiplayer";
        HttpEntity<RequestMultiPlayers> requestEntity = new HttpEntity<>(request);
        ResponseEntity<PracticeData[]> entity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, PracticeData[].class);
        return entity.getBody();
    }
    public PracticeData[] getTeam(RequestTeam request){
        String url = "http://INFORMATION/api/v1/info/practice/team";
        HttpEntity<RequestTeam> requestEntity = new HttpEntity<>(request);
        ResponseEntity<PracticeData[]> entity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, PracticeData[].class);
        return entity.getBody();
    }

    public PracticeData getLastPlayer(long playerID){
        String url = String.format("http://INFORMATION/api/v1/info/get/%o",playerID);
        ResponseEntity<PracticeData> entity = restTemplate.getForEntity(url, PracticeData.class);
        return entity.getBody();
    }

}
