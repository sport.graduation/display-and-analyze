package com.example.display;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.hystrix.EnableHystrix;
//import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

//@EnableHystrix
//@EnableHystrixDashboard
//@EnableDiscoveryClient
@EnableEurekaClient
@SpringBootApplication
public class DisplayAndAnalyzeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DisplayAndAnalyzeApplication.class, args);
	}

/*	@Bean
	public Sampler samplerOb() {
		return Sampler.ALWAYS_SAMPLE;
	}*/

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
