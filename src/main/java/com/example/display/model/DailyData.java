package com.example.display.model;


import lombok.*;



@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor


public class DailyData {
    private long id;
    private long playerID;
    private long teamID;
    private double relaxedHeartRate;
    private double lightIntensityHeartRate;
    private double moderateIntensityHeartRate;
    private double vigorousIntensityHeartRate;
    private double sleepHours;
}
