package com.example.display.model;


import com.example.display.model.analyzeddata.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class AnalyzedPracticeData {
    private long playerID;
    private List<TotalDistance> totalDistanceList;
    private String totalDistanceImprovement;
    private List<MaxSpeed> maxSpeedList;
    private String maxSpeedImprovement;
    private List<BurnedCalories> burnedCaloriesList ;
    private String burnedCaloriesImprovement;
    private List<NumOfSprints> numOfSprintsList ;
    private String numOfSprintsImprovement;
    private List<HSR> hsrList ;
    private String hsrImprovement;
    private List<AvgHeartRate> avgHeartRateList;
    private String avgHeartRateImprovement;
    private List<MaxHeartRate> maxHeartRateList;
    private String maxHeartRateImprovement;
    private List<MinHeartRate> minHeartRateList;
    private String minHeartRateImprovement;
    private List<RedZoneDuration> redZoneDurationList;
    private String redZoneDurationImprovement;
}
