package com.example.display.model.analyzeddata;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class HSR {
    private double hsr ;

    private long startTimeMillis;
    private long endTimeMillis;
}
