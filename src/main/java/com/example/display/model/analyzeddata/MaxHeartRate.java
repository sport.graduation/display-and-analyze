package com.example.display.model.analyzeddata;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MaxHeartRate {
    private int maxHeartRate;

    private long startTimeMillis;
    private long endTimeMillis;
}
