package com.example.display.model.analyzeddata;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class NumOfSprints {
    private int numOfSprints ;

    private long startTimeMillis;
    private long endTimeMillis;
}
