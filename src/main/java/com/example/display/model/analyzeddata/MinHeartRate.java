package com.example.display.model.analyzeddata;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MinHeartRate {
    private int minHeartRate;

    private long startTimeMillis;
    private long endTimeMillis;
}
