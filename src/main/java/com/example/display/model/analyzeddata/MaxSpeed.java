package com.example.display.model.analyzeddata;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MaxSpeed {
    private double maxSpeed;

    private long startTimeMillis;
    private long endTimeMillis;
}
