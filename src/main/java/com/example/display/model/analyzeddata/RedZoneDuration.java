package com.example.display.model.analyzeddata;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RedZoneDuration {
    private int redZoneDuration;
    private long startTimeMillis;
    private long endTimeMillis;
}
