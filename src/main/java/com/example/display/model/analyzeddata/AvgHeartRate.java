package com.example.display.model.analyzeddata;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AvgHeartRate {
    private int avgHeartRate;
    private long startTimeMillis;
    private long endTimeMillis;
}
