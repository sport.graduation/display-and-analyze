package com.example.display.model.analyzeddata;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TotalDistance {
    private int totalDistance;

    private long startTimeMillis;
    private long endTimeMillis;
}
