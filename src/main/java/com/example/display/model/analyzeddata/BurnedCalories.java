package com.example.display.model.analyzeddata;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BurnedCalories {
    private int burnedCalories ;

    private long startTimeMillis;
    private long endTimeMillis;
}
