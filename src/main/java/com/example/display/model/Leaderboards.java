package com.example.display.model;

import com.example.display.model.leaderboards.HsrLeaderboards;
import com.example.display.model.leaderboards.MaxSpeedLeaderboards;
import com.example.display.model.leaderboards.TotalDistanceLeaderboards;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Leaderboards {
    private List<MaxSpeedLeaderboards> maxSpeedLeaderboards;
    private List<TotalDistanceLeaderboards> totalDistanceLeaderboards;
    private List<HsrLeaderboards> hsrLeaderboards;

}

