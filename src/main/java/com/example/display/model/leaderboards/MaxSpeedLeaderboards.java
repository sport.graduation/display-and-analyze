package com.example.display.model.leaderboards;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MaxSpeedLeaderboards {
    private long playerID;
    private double maxSpeed;
}