package com.example.display.model.leaderboards;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TotalDistanceLeaderboards {
    private long playerID;
    private double totalDistance;
}
