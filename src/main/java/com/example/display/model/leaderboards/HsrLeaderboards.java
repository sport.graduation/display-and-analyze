package com.example.display.model.leaderboards;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class HsrLeaderboards {
    private long playerID;
    private double hsr;
}