package com.example.display.controller;

import com.example.display.dto.RequestTeam;
import com.example.display.model.Leaderboards;
import com.example.display.service.LeaderBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/display")
public class LeaderBoardController {

    @Autowired
    LeaderBoardService service;

   // @HystrixCommand(fallbackMethod = "alter", commandProperties ={@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),})
    @PostMapping("/leaderboard")
    public Leaderboards showSpecificPlayerDailyData(@RequestBody RequestTeam requestTeam){
        Leaderboards leaderboardsList = service.leaderboards(requestTeam);
        return leaderboardsList;
    }

    /*public Leaderboards alter(RequestTeam requestTeam)
    {
        return "service is down";
    }*/
}
