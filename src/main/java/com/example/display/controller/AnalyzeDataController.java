package com.example.display.controller;

import com.example.display.dto.RequestForAnalyze;
import com.example.display.dto.RequestMultiPlayers;
import com.example.display.dto.RequestOnePlayer;
import com.example.display.model.AnalyzedPracticeData;
import com.example.display.model.PracticeData;
import com.example.display.service.AnalyzeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api/v1/display")
public class AnalyzeDataController {

    @Autowired
    AnalyzeDataService analyzeDataService;

    @PostMapping("/analyzed")
    public ResponseEntity<AnalyzedPracticeData> showPlayerAnalyzedPracticeData(@RequestBody RequestForAnalyze request){
        AnalyzedPracticeData data = analyzeDataService.analyzeData(request);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }
}
