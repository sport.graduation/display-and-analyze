package com.example.display.controller;
import com.example.display.dto.RequestMultiPlayers;
import com.example.display.dto.RequestOnePlayer;
import com.example.display.dto.RequestTeam;
import com.example.display.model.DailyData;
import com.example.display.model.PracticeData;
import com.example.display.service.PlayerDailyDataService;
import com.example.display.service.PlayerPracticeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/api/v1/display")
public class DisplayInfoController {

    @Autowired
    PlayerPracticeDataService playerPracticeDataService;
    @Autowired
    PlayerDailyDataService playerDailyDataService;

    @PostMapping("/practice/one")
    public ResponseEntity<PracticeData> showSpecificPlayerPracticeData(@RequestBody RequestOnePlayer request){
        PracticeData data = playerPracticeDataService.getOnePlayer(request);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/daily/one")
    public ResponseEntity<DailyData> showSpecificPlayerDailyData(@RequestBody RequestOnePlayer request){
        DailyData data = playerDailyDataService.getOnePlayer(request);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/practice/multiplayer")
    public ResponseEntity<List<PracticeData>> showMultiplePlayerPracticeData(@RequestBody RequestMultiPlayers request){
        List<PracticeData> data = List.of(playerPracticeDataService.getMultiplePlayer(request));
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/daily/multiplayer")
    public ResponseEntity<List<DailyData>> showMultiplePlayerDailyData(@RequestBody RequestMultiPlayers request){
        List<DailyData> data = List.of(playerDailyDataService.getMultiplePlayer(request));
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/practice/team")
    public ResponseEntity<List<PracticeData>> showTeamPracticeData(@RequestBody RequestTeam request){
        List<PracticeData> data = List.of(playerPracticeDataService.getTeam(request));
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/daily/team")
    public ResponseEntity<List<DailyData>> showTeamDailyData(@RequestBody RequestTeam request){
        List<DailyData> data = List.of(playerDailyDataService.getTeam(request));
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @GetMapping("/practice/last/{id}")
    public ResponseEntity<PracticeData> showLastPracticeData(@PathVariable("id") long playerID){
        PracticeData data = playerPracticeDataService.getLastPlayer(playerID);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }


}
