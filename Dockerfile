FROM openjdk:11.0.16-oracle

WORKDIR /app

EXPOSE 9400

COPY target/*.jar display.jar

ENTRYPOINT [ "java","-jar","./display.jar"]